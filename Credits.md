# Credits
First: Thanks to the authors from [wiki.vg](https://wiki.vg) for documenting the protocol. Also, special thanks to the `#mcdevs` irc, there are many great people that helped us.

## Thanks to the following projects:

- [Mojang](https://mojang.com) For the original game. Many people (including me) blame mojang for making "bad decisions". imho that is kind of true. I know the protocol is a mess at some point, but also genius on the other side. The thing is, minecraft is more than a protocol. While reversing the game, I saw so much stuff. The original game has around 20 Mib of code. That is immense. Every different particle behaves different and has so much thinking in it. It is incredible, that somebody
  thought, that the campfire particle has a velocity of 0.81 in y direction, etc
  (just an example, not quite true). I never thought of it. I learned so much while writing minosoft and want to thank mojang for it. The game is just fascinating, in players view and even more in developer view.
- [PixLyzer](https://gitlab.bixilon.de/bixilon/pixlyzer)
- [Burger](https://github.com/Pokechu22/Burger)
- [wiki.vg](https://wiki.vg)

## Thanks to all contributors:

- [Lukas](https://gitlab.bixilon.de/lukas) For giving me motivation and developing part of the rendering and doing some voodoo math for me :)
- [Alex](https://gitlab.bixilon.de/alexamg) For the awesome logo
