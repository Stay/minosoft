package de.bixilon.minosoft.config.profile.profiles.controls

import de.bixilon.minosoft.modding.event.events.Event

class ControlsProfileSelectEvent(
    val profile: ControlsProfile,
) : Event
