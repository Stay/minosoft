package de.bixilon.minosoft.config.profile.profiles.resources

import de.bixilon.minosoft.modding.event.events.Event

class ResourcesProfileSelectEvent(
    val profile: ResourcesProfile,
) : Event
