package de.bixilon.minosoft.config.profile.profiles.connection

import de.bixilon.minosoft.modding.event.events.Event

class ConnectionProfileSelectEvent(
    val profile: ConnectionProfile,
) : Event
