package de.bixilon.minosoft.config.profile.profiles.rendering.fog

import de.bixilon.minosoft.config.profile.profiles.rendering.RenderingProfileManager.delegate

class FogC {

    /**
     * Enables or disables fog
     */
    var enabled by delegate(true)
}
