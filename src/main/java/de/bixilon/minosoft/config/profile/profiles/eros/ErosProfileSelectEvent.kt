package de.bixilon.minosoft.config.profile.profiles.eros

import de.bixilon.minosoft.modding.event.events.Event

class ErosProfileSelectEvent(
    val profile: ErosProfile,
) : Event
