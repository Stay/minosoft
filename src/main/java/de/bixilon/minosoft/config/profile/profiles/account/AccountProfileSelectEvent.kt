package de.bixilon.minosoft.config.profile.profiles.account

import de.bixilon.minosoft.modding.event.events.Event

class AccountProfileSelectEvent(
    val profile: AccountProfile,
) : Event
