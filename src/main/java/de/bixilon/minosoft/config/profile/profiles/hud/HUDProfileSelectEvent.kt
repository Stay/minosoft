package de.bixilon.minosoft.config.profile.profiles.hud

import de.bixilon.minosoft.modding.event.events.Event

class HUDProfileSelectEvent(
    val profile: HUDProfile,
) : Event
