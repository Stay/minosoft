package de.bixilon.minosoft.config.profile.profiles.block

import de.bixilon.minosoft.modding.event.events.Event

class BlockProfileSelectEvent(
    val profile: BlockProfile,
) : Event
