package de.bixilon.minosoft.config.profile.profiles.audio.types

import de.bixilon.minosoft.config.profile.profiles.audio.AudioProfileManager.delegate

class TypesC {

    /**
     * Play (custom) sounds from the server
     */
    var packet by delegate(true)
}
