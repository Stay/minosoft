package de.bixilon.minosoft.config.profile.profiles.resources.assets.packs

class ResourcePack(
    var type: ResourcePackType,
    var path: String,
)
