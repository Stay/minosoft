package de.bixilon.minosoft.config.profile.profiles.entity

import de.bixilon.minosoft.modding.event.events.Event

class EntityProfileSelectEvent(
    val profile: EntityProfile,
) : Event
