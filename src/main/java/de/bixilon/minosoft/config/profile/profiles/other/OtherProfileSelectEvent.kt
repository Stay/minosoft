package de.bixilon.minosoft.config.profile.profiles.other

import de.bixilon.minosoft.modding.event.events.Event

class OtherProfileSelectEvent(
    val profile: OtherProfile,
) : Event
