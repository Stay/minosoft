package de.bixilon.minosoft.config.profile.profiles.rendering.animations

import de.bixilon.minosoft.config.profile.profiles.rendering.RenderingProfileManager.delegate

class AnimationsC {

    /**
     * Enables or disables sprite animations.
     */
    var sprites by delegate(true)
}
