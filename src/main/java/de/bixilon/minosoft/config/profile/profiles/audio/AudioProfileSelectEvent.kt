package de.bixilon.minosoft.config.profile.profiles.audio

import de.bixilon.minosoft.modding.event.events.Event

class AudioProfileSelectEvent(
    val profile: AudioProfile,
) : Event
