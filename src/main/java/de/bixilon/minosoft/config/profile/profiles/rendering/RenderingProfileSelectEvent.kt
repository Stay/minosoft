package de.bixilon.minosoft.config.profile.profiles.rendering

import de.bixilon.minosoft.modding.event.events.Event

class RenderingProfileSelectEvent(
    val profile: RenderingProfile,
) : Event
