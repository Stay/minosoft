package de.bixilon.minosoft.config.profile.profiles.particle

import de.bixilon.minosoft.modding.event.events.Event

class ParticleProfileSelectEvent(
    val profile: ParticleProfile,
) : Event
