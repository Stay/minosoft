package de.bixilon.minosoft.data.language

annotation class Description(
    val nameKey: String,
    val translationKey: String,
)
