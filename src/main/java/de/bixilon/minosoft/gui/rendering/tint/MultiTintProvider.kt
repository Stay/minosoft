package de.bixilon.minosoft.gui.rendering.tint

interface MultiTintProvider {
    val tints: Int
}
