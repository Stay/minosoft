package de.bixilon.minosoft.gui.rendering.system.base.phases

interface SkipAll {
    val skipAll: Boolean
}
