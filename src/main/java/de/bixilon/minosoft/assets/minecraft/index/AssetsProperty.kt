package de.bixilon.minosoft.assets.minecraft.index

data class AssetsProperty(
    val type: IndexAssetsType,
    val hash: String,
    val size: Long,
)
